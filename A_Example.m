%Developper     : Jesus-Pablo Toledo-Zucco
%Date           : 02/02/2024
%% PI design for a piece-wise linear model with different delays
%This example shows how one can improve the closed-loop behaviour of a
%piece-wise linear model that swtiches between two different linear models
%with different time delays.

%The objective is to dessing a single PI controller that stabilizes both
%models.

%In this code we first show the open loop response. Then, we show a
%closed-loop response with a PI controller in which the parameters are
%tunned using a conventional approach (Amigo rules). Using the conventional
%approach, the design is based in the worst case.

%The proposed tuning rules for the PI controller considers both cases and
%can improve the closed-loop behaviour of the case that was not considered
%using the conventional approach.

close all
clear all
clc


%% Define the piecewise linear models

%Sigma = 1. Worst case in terms of delay
a1 = 0.2; b1 = 1.2; d1 = 6;

%Sigma = 2
a2 = 0.8; b2 = 0.9; d2 = 1;

h = d1-d2;

%% Open-Loop Step Responses
%In the open-loop simulation one can see the behaviour of the swtiched
%system. When the step is positive, the response has a big delay. When, the
%step is negative, the response has a small delay.
OL_Sim

%% Closed-Loop Step Responses conventional design
%Using a conventional approach, one has to select one model to tune the PI
%parameters. In this case, we select the one with the worst delay. One can
%see a fast with almost no overshoot for the positive step response.
%However, the negative step response has an overshoot ans some
%oscillations.
CL_Sim1

%% Proposed Tuning Rules
%Using the proposed tuning rules, the design is based on both models,
%guaranting the closed-loop stability in both scenarios. One can then 
%improve the behaviour in the model that has not been considered for the
%conventional approach.

%There are two adventages of the proposed approach:
%1. The LMI in Step 3 can be used to guarantee closed-loop stability when
%using a conventional approach and haveing a swtiching behaviour (2 possible models) 

%2. The design penalizes the proportional and/or the integral term of the
%closed-loop system.

%The main disventage of the proposed approach is that until now, the
%searching method uses a grid of parameters (kp,ki) to find an optimal couple.
%A possible contribution can be an approach that searches (kp,ki) starting
%from some initial value and moves in the direciton of minimizing the cost
%function.
%% Step 1: Select the ranges of kp and ki
%The parameters to set are the boundaries of kp and ki.
kp_min = 0; kp_max = 0.8; %Range kp
ki_min = 0; ki_max = 0.2;  %Range ki

%The size of the grid. if L is small less parameters are taked into account
%and the solution is obtained faster.
L = 20; %Size of the grid.... Less than one minutes for solving!
% L = 40; %Size of the grid... 10 minutes for solving!
gain_Q = 2;
Q = gain_Q*diag([1,0,1,0]);


%% Step 2: Generate the grid
kp_range = linspace(kp_min,kp_max,L); %Vector kp range
ki_range = linspace(ki_min,ki_max,L); %Vector ki range
[Kp,Ki] = meshgrid(kp_range,ki_range);%Generate the grid

%% Step 3: Solve LMI (3) for every pair in the grid
N = 4; % Size of the closed-loop matrices
I_N = eye(N);	% Identity matrix
Z_N = zeros(N);	% Null matrix
k = 0;
for i = 1:L
    for j = 1:L
        % Move the design parameters along the grid
        kp = Kp(i,j);
        ki = Ki(i,j);
                
        %Actualize the closed-loop matrices for the new controller
        %parameters
        A1_tilde = [a1,0,-b1*kp,-b1*ki;
                     1,1,0,0;
                     1,0,0,0;
                     0,1,0,0];
        A2_tilde = [a2,0,-b2*kp,-b2*ki;
                     1,1,0,0;
                     1,0,0,0;
                     0,1,0,0];
        Ap_tilde = -[0,0,b1*kp,b1*ki;
                    0,0,0,0;
                    0,0,0,0;
                    0,0,0,0];


        % Solve the LMI
        setlmis([])
        [P1,np,sP1] = lmivar(1,[N 1]);
        [P2,np,sP2] = lmivar(1,[N 1]);
        [P3,np,sP3] = lmivar(1,[N 1]);
        [P,np,sP] = lmivar(3,[sP1,Z_N;sP2,sP3]);
        [S1,np,sS1] = lmivar(1,[N 1]);
        [S2,np,sS2] = lmivar(1,[N 1]);
        [W1,np,sW1] = lmivar(1,[N 1]);
        [W2,np,sW2] = lmivar(1,[N 1]);
        [W3,np,sW3] = lmivar(1,[N 1]);
        [W,np,sW] = lmivar(3,[sW1,sW2;sW2',sW3]);
        [M1,np,sM1] = lmivar(1,[N 1]);
        [M2,np,sM2] = lmivar(1,[N 1]);
        [M,np,sM] = lmivar(3,[sM1;sM2]);

        % P1 > 0
        lmiterm([-1 1 1 P],[I_N,Z_N],[I_N;Z_N])
        % S1 > 0
        lmiterm([-2 1 1 S1],1,1)
        %S2 > 0
        lmiterm([-3 1 1 S2],1,1)
        %[W,M;M^T,S1] >0
        lmiterm([-4 1 1 W],1,1)
        lmiterm([-4 1 2 M],1,1)
        lmiterm([-4 2 2 S1],1,1)
        %Lambda < 0
        lmiterm([5 1 1 W],h,1)
        lmiterm([5 1 1 S2],[I_N;Z_N],[I_N,Z_N])
        lmiterm([5 1 1 P],[Z_N;I_N]*[I_N,Z_N],[I_N;Z_N]*[Z_N,I_N])
        lmiterm([5 1 1 S1],h*[Z_N;I_N],[Z_N,I_N])
        lmiterm([5 1 1 M],1,[I_N,Z_N])
        lmiterm([5 1 1 -M],[I_N;Z_N],1)    
        lmiterm([5 1 1 -P],1,[Z_N,I_N;A1_tilde-Ap_tilde-I_N,-I_N])
        lmiterm([5 1 1 P],[Z_N,A1_tilde'-Ap_tilde'-I_N;I_N,-I_N],1)
        lmiterm([5 1 2 -P],1,[Z_N;Ap_tilde])
        lmiterm([5 1 2 M],-1,1)
        lmiterm([5 2 2 S2],-1,1)
%         lmiterm([-5 1 1 0],-0*[Q,Z_N;Z_N,Z_N])
        %Mode2
        % A2_tilde^T P1 A2_tilde - P1 < 0
        lmiterm([6 1 1 P],A2_tilde'*[I_N,Z_N],[I_N;Z_N]*A2_tilde)
        lmiterm([6 1 1 P],-[I_N,Z_N],[I_N;Z_N])
%         lmiterm([-6 1 1 0],-Q*0)
        %Lambda_2 < 0
        lmiterm([7 1 1 S1],h*(A2_tilde')^h,A2_tilde^h)
        for iii = 0:h-1
            lmiterm([7 1 1 S1],-(A2_tilde')^iii,A2_tilde^iii)
        end
        lmiterm([7 1 1 S2],(A2_tilde')^h,A2_tilde^h)
        lmiterm([7 1 1 S2],-1,1)

        lmisys = getlmis;

        [t,xx] = feasp(lmisys);
        
        %Save the set Omega1
        if t<0
            k = k+1;  
            kp_Omega1(k) = kp;
            ki_Omega1(k) = ki;
        end
    end
end

% save('Omega_1','kp_Omega1','ki_Omega1')


%% Step 4: Optimal selection
k = 0;
for i = 1:length(kp_Omega1)
        % Actualize controller parameters inside Omega1
        kp = kp_Omega1(i);
        ki = ki_Omega1(i);
                
        %Define the closed-loop matrices
        A1_tilde = [a1,0,-b1*kp,-b1*ki;
                     1,1,0,0;
                     1,0,0,0;
                     0,1,0,0];
        A2_tilde = [a2,0,-b2*kp,-b2*ki;
                     1,1,0,0;
                     1,0,0,0;
                     0,1,0,0];
        Ap_tilde = -[0,0,b1*kp,b1*ki;
                    0,0,0,0;
                    0,0,0,0;
                    0,0,0,0];


        % Initialize the LMI
        setlmis([])
        [P1,np,sP1] = lmivar(1,[N 1]);
        [P2,np,sP2] = lmivar(1,[N 1]);
        [P3,np,sP3] = lmivar(1,[N 1]);
        [P,np,sP] = lmivar(3,[sP1,Z_N;sP2,sP3]);
        [S1,np,sS1] = lmivar(1,[N 1]);
        [S2,np,sS2] = lmivar(1,[N 1]);
        [W1,np,sW1] = lmivar(1,[N 1]);
        [W2,np,sW2] = lmivar(1,[N 1]);
        [W3,np,sW3] = lmivar(1,[N 1]);
        [W,np,sW] = lmivar(3,[sW1,sW2;sW2',sW3]);
        [M1,np,sM1] = lmivar(1,[N 1]);
        [M2,np,sM2] = lmivar(1,[N 1]);
        [M,np,sM] = lmivar(3,[sM1;sM2]);

%         % P1 > 0
%         lmiterm([-1 1 1 P],[I_N,Z_N],[I_N;Z_N])
%         % S1 > 0
%         lmiterm([-2 1 1 S1],1,1)
%         % S2 > 0
%         lmiterm([-3 1 1 S2],1,1)
%         % [W,M;M^T,S1] >0
%         lmiterm([-4 1 1 W],1,1)
%         lmiterm([-4 1 2 M],1,1)
%         lmiterm([-4 2 2 S1],1,1)
%         % Lambda < -Q_bar
%         lmiterm([5 1 1 W],h,1)
%         lmiterm([5 1 1 S2],[I_N;Z_N],[I_N,Z_N])
%         lmiterm([5 1 1 P],[Z_N;I_N]*[I_N,Z_N],[I_N;Z_N]*[Z_N,I_N])
%         lmiterm([5 1 1 S1],h*[Z_N;I_N],[Z_N,I_N])
%         lmiterm([5 1 1 M],1,[I_N,Z_N])
%         lmiterm([5 1 1 -M],[I_N;Z_N],1)    
%         lmiterm([5 1 1 -P],1,[Z_N,I_N;A1_tilde-Ap_tilde-I_N,-I_N])
%         lmiterm([5 1 1 P],[Z_N,A1_tilde'-Ap_tilde'-I_N;I_N,-I_N],1)
%         lmiterm([5 1 2 -P],1,[Z_N;Ap_tilde])
%         lmiterm([5 1 2 M],-1,1)
%         lmiterm([5 2 2 S2],-1,1)
%         lmiterm([-5 1 1 0],-[Q,Z_N;Z_N,Z_N])
%         %Mode 2
%         % A2_tilde^T P1 A2_tilde - P1 < -Q
%         lmiterm([6 1 1 P],A2_tilde'*[I_N,Z_N],[I_N;Z_N]*A2_tilde)
%         lmiterm([6 1 1 P],-[I_N,Z_N],[I_N;Z_N])
%         lmiterm([-6 1 1 0],-Q)
%         %Lambda_2 < 0
%         lmiterm([7 1 1 S1],h*(A2_tilde')^h,A2_tilde^h)
%         for iii = 0:h-1
%             lmiterm([7 1 1 S1],-(A2_tilde')^iii,A2_tilde^iii)
%         end
%         lmiterm([7 1 1 S2],(A2_tilde')^h,A2_tilde^h)
%         lmiterm([7 1 1 S2],-1,1)
% %         lmiterm([7 1 1 0],-Q)
        
        %LMI 1
        lmiterm([-1 1 1 P],[I_N,Z_N],[I_N;Z_N])
        %LMI 2
        lmiterm([-2 1 1 S1],1,1)
        %LMI 3
        lmiterm([-3 1 1 S2],1,1)
        %LMI 4
        lmiterm([-4 1 1 W],1,1)
        lmiterm([-4 1 2 M],1,1)
        lmiterm([-4 2 2 S1],1,1)
        %LMI 5
        lmiterm([5 1 1 W],h,1)
        lmiterm([5 1 1 S2],[I_N;Z_N],[I_N,Z_N])
        lmiterm([5 1 1 P],[Z_N;I_N]*[I_N,Z_N],[I_N;Z_N]*[Z_N,I_N])
        lmiterm([5 1 1 S1],h*[Z_N;I_N],[Z_N,I_N])
        lmiterm([5 1 1 M],1,[I_N,Z_N])
        lmiterm([5 1 1 -M],[I_N;Z_N],1)    
        lmiterm([5 1 1 -P],1,[Z_N,I_N;A1_tilde-Ap_tilde-I_N,-I_N])
        lmiterm([5 1 1 P],[Z_N,A1_tilde'-Ap_tilde'-I_N;I_N,-I_N],1)
        lmiterm([5 1 2 -P],1,[Z_N;Ap_tilde])
        lmiterm([5 1 2 M],-1,1)
        lmiterm([5 2 2 S2],-1,1)
        lmiterm([-5 1 1 0],-[Q,Z_N;Z_N,Z_N])
        %LMI 6
        lmiterm([6 1 1 P],A2_tilde'*[I_N,Z_N],[I_N;Z_N]*A2_tilde)
        lmiterm([6 1 1 P],-[I_N,Z_N],[I_N;Z_N])
        lmiterm([-6 1 1 0],-Q)

        lmiterm([7 1 1 S1],h*(A2_tilde')^h,A2_tilde^h)
        for iii = 0:h-1
            lmiterm([7 1 1 S1],-(A2_tilde')^iii,A2_tilde^iii)
        end
        lmiterm([7 1 1 S2],(A2_tilde')^h,A2_tilde^h)
        lmiterm([7 1 1 S2],-1,1)
        lmiterm([7 1 1 0],-Q)



        lmisys = getlmis;

        [t,xx] = feasp(lmisys);

        %Save the set Omega2
        if t<0
            k = k+1;
            
            kp_Omega2(k) = kp;
            ki_Omega2(k) = ki;
            
            P1_p = [I_N,Z_N]*dec2mat(lmisys,xx,P)*[I_N;Z_N];
            S2_p = dec2mat(lmisys,xx,S2);
            S1_p = dec2mat(lmisys,xx,S1);
            lambda_max(k) = max(abs(eig(P1_p + h*S2_p)));
            trace_P1hS2(k) = trace(P1_p + h*S2_p);


        end
end


%% Step 5: Select the the pair (kp,ki) such that the trace of P1 + hS2 is minimized
[lambda_min,k_lambda] = min(lambda_max);
[trace_min,k_trace] = min(trace_P1hS2);

% Optimal pair (kp,ki)
kp = kp_Omega2(k_trace);
ki = ki_Omega2(k_trace);

%% Closed-Loop Simulation: Proposed design
CL_Sim2

%% Plot the regions and the optimal value

font=40; axisfont=font*0.6; lw=6; ms = 40;

x0screen=100;
y0screen=100;
WidthScreen=1000;
HeightScreen=300;
Blue    = [0 0.4470 0.7410];
Orange  = [0.8500 0.3250 0.0980];
Yellow  = [0.9290 0.6940 0.1250];
Violet  = [0.4940 0.1840 0.5560];
Green   = [0.4660 0.6740 0.1880];


xoff = -0.066;
yoff = 0.06;
widthoff = 0.14;
heightoff = -0.03;

figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,1.8*HeightScreen])
hold on
plot(kp_Omega1,ki_Omega1,'s','LineWidth',lw,'MarkerSize',ms)
plot(kp_Omega2,ki_Omega2,'.','LineWidth',lw,'MarkerSize',ms)
plot(kp_Omega2(k_trace),ki_Omega2(k_trace),'*','LineWidth',lw/2,'MarkerSize',ms,'Color',Green)
plot(kp_amigo,ki_amigo,'*','LineWidth',lw/2,'MarkerSize',ms)
set(gca,'FontSize',axisfont);

legend('$(k_p,k_i) \in \Omega_1$','$(k_p,k_i) \in \Omega_2$','$(k_p,k_i)$ Proposed','$(k_p,k_i)$ Amigo','Interpreter','latex')
grid on
xlabel('$k_p$','Interpreter','latex','FontSize',font)
ylabel('$k_i$','Interpreter','latex','FontSize',font)


% save('Omega_2','kp_Omega2','ki_Omega2')
% 
% save('Optimal','kp','ki')
