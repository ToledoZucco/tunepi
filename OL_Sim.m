TOL     = 0.05;
dt      = 1;
t       = 0:dt:1*100;
x0      = 0;
u0      = 0;
sig0    = 1; 
x       = zeros(1,length(t));
u       = zeros(1,length(t));
sigma   = zeros(1,length(t))+sig0;

t_up    = 10;
t_down  = 50;
% u = heaviside(t-t_up) - heaviside(t-t_down);

for i = d1+1:length(t)
    
    if t(i)> t_up && t(i)< t_down
        u(i) = 1;
    else
        u(i) = 0;
    end
    
    if t(i)> t_down
        sigma(i) = 2;
    end
    
    if sigma(i) == 1
            a = a1;
            b = b1;
            d = d1;
    elseif sigma(i) == 2
            a = a2;
            b = b2;
            d = d2;
    end
     
        x(i+1) = a*x(i) + b*u(i-d);
end
x = x(1:end-1);


%% Plots

font=34; axisfont=font*0.5; lw=6; ms = 10;

x0screen=100;
y0screen=100;
WidthScreen=1000;
HeightScreen=300;
Blue = [0 0.4470 0.7410];
Orange = [0.8500 0.3250 0.0980];
Yellow = [0.9290 0.6940 0.1250];
Violet = [0.4940 0.1840 0.5560];
Green = [0.4660 0.6740 0.1880];
Cyan = [0.3010 0.7450 0.9330];
Red = [1 0 0];

xoff = -0.05;
yoff = 0.05;
widthoff = 0.12;
heightoff = -0.1;

figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,1.4*HeightScreen])
hold on
plot(t,u,'o','LineWidth',lw,'MarkerSize',ms)
plot(t,x,'x','LineWidth',lw,'MarkerSize',ms)
plot(t,sigma,'.','LineWidth',lw,'MarkerSize',ms*2)
set(gca,'FontSize',axisfont);
grid on
Pos = get(gca,'Position');
set(gca,'Position',Pos+[xoff yoff widthoff heightoff])
legend({'$u_k$','$x_k$','$\sigma_k$'},'Location','northeast','Interpreter','latex','FontSize',font)
xlabel({' $t$ $[min]$'},'Interpreter','latex','FontSize',font)
ylabel({'Signals'},'Interpreter','latex','FontSize',font*1)
title({'Open-Loop Response'},'Interpreter','latex','FontSize',font)
print(gcf,'-dpng','-r200','Figs/OL_Response.png');

